<?php

require_once("../vendor/autoload.php");

use GuzzleHttp\Client;

$message = "";
if(isset($_GET["pays"])) {
	$pays = trim($_GET["pays"]);
	$message .= "<h1>$pays</h1>";
	
	$url = "https://fr.wikipedia.org/w/api.php?action=parse&page=".
		$pays."&format=json";
	$client_http = new Client();
	$reponse_http = $client_http->request('GET', $url);
	if($reponse_http->getStatusCode() == 200) {
		$racine = json_decode($reponse_http->getBody());
		$texte = $racine->parse->text->{"*"};
		$message .= substr(strip_tags($texte), 0, 1000);
	}
}
?><html>
<body>
	<form>
		<p>Pays : <input name="pays">
			<input type="submit">
		</p>
	</form>
	<p><?= $message ?></p>
</body>
</html>
		