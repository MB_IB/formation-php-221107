<?php

/*
nom, le slogan, les villes (d'implantation), et le logo de l'agence,
le logo étant une lettre, une couleur de fond (3 composantes - chacune)
entre 0 et 255) et une couleur de texte (3 composantes) et un nom 
de police
*/

$agence = [ 
	"nom" => "Toulouse Voyage",
	"slogan" => "Voyages de la ville rose",
	"villes" => [ "Toulouse", "Tarbes", "Carcassonne"],
	"logo" => [
		"lettre" => "T",
		"fond" => [255, 70, 90],  // RGB
		"texte" => [255, 255, 255],
		"police" => "Tahoma"
	]
];
{
	var_dump($agence);
	echo "<br>Première ville d'implantation : ". $agence["villes"][0];

	$lum_fond = 0;
	foreach($agence["logo"]["fond"] as $c)
		$lum_fond += $c;
	$lum_texte = 0;
	foreach($agence["logo"]["texte"] as $c)
		$lum_texte += $c;
	if($lum_texte >= $lum_fond)
		echo "<br>Lettre claire sur fond sombre";
	else
		echo "<br>Lettre sombre sur fond clair";

	$agence["logo"]["fond"][1] -= 10; // vert un peu plus sombre
	$agence["villes"] []= "Albi";
	unset($agence["villes"][1]);
	echo "<br>Ville 2 : ".$agence["villes"][2];
	var_dump($agence);
}

function affiche_nom($a) {
	echo "<br>Nom : ".$a["nom"];
}
affiche_nom($agence);

function met_nom_en_majuscules(& $a) {
	$a["nom"] = strtoupper( $a["nom"] );
}
met_nom_en_majuscules ($agence) ;
echo "<br>Nom en majuscules : ".$agence["nom"];

function met_nom_en_minuscules($a) {
	$a["nom"] = strtolower( $a["nom"] );
	return $a;
}
$agence = met_nom_en_minuscules ($agence) ;
echo "<br>Nom en minuscules : ".$agence["nom"];

$implantations = [2024=>"Limoges", 2026=>"New York ï", 
	2023=>"Lourdes"];
asort($implantations); // par valeur
ksort($implantations); // par clé	
function compare_longueur($a, $b) {
	if(strlen($a)<strlen($b))
		return -1;
	elseif(strlen($a)>strlen($b))
		return 1;
	else
		return 0;
}
uasort($implantations, "compare_longueur"); //"user associative sort"
echo "<br>";
var_dump($implantations);

$i_avec_espace = array_filter(
	$implantations, 
	function($v) { return strpos($v, " ")!==false; } 
);
echo "<br>Avec espace(s) : ";
var_dump($i_avec_espace);


$i3 = array_map(function($v) { return strlen($v); }, $implantations);
echo "<br>Longueur des noms de villes : ";
var_dump($i3);

$nb_lettres = array_reduce($implantations, 
	function($total, $v) { 
		if($total == null)
			$total = 0;
		return $total + mb_strlen($v);
	}
);
echo "<br>Nombre de lettres des noms de villes : ".$nb_lettres;

$plus_grand = array_reduce($implantations, 
	function($max, $v) { 
		if($max == null)
			$total = "";
		if(strlen($max)<strlen($v))
			$max = $v;
		return $max;
	}
);
echo "<br>Plus grand nom de ville : ".$plus_grand;


