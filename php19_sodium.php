<?php

// inscription d'un voyageur usager
$motdepasse = "1234"; // ou lire dans $_POST
$user_id = 45; // ou lire dans la base
$salt = $user_id."voyagestoulouse";
$h1 = sodium_crypto_generichash($motdepasse.$salt); //BLAKE2b
$h1b64 = sodium_bin2base64($h1, SODIUM_BASE64_VARIANT_ORIGINAL);
echo $h1b64."<br>"; // ou enregistrer dans la base

// signature : chiffrement avec clé privée, déchiffrement avec clé publique
// secret : chiffrement avec clé publique, déchiffrement avec clé privée
// secret : chiffrement symétrique

$message_secret = "Ne pas prendre l'A320 GH2342, il est cassé";
$key = sodium_crypto_aead_xchacha20poly1305_ietf_keygen();
echo "Clé : ".sodium_bin2base64($key, SODIUM_BASE64_VARIANT_ORIGINAL)."<br>";
$nonce = random_bytes(24);
$crypte = sodium_crypto_aead_xchacha20poly1305_ietf_encrypt(
	$message_secret, "", $nonce, $key
);
echo "Message chiffré : ".sodium_bin2base64($crypte,
	SODIUM_BASE64_VARIANT_ORIGINAL)."<br>";

$decrypte = sodium_crypto_aead_xchacha20poly1305_ietf_decrypt(
	$crypte, "", $nonce, $key
);
echo "Message déchiffré : ".$decrypte."<br>";
