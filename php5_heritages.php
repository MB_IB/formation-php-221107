<?php

interface Affichable {
	public function afficher();
}

abstract class Produit {
	public $nom, $prix;	
	public function __construct($n = null, $p = 0) {
		$this->nom = $n;
		$this->prix = $p;		
	}
	abstract public function afficher();
}

trait ObjetNomme {
	public function metNomEnMajuscules() {
		$this->nom = strtoupper($this->nom);
	}
}

class Voyage extends Produit implements Affichable {
	use ObjetNomme;
	public function afficher() {
		echo "<p>".$this->nom." (".$this->prix."€)</p>";
	}	
}

class Client {
	use ObjetNomme { ObjetNomme::metNomEnMajuscules as renomme; }
	public $prenom, $nom;
}

// $p1 = new Produit("Pomme", 0.5); //  Cannot instantiate abstract class
$v1 = new Voyage("Merveilles de Mediterranée", 1210);
$v1->metNomEnMajuscules();
$v1->afficher();

function presenter(Affichable $o) {
	echo "<div style='border:1px solid gray'>";
	$o->afficher();
	echo "</div>";
}
presenter($v1);
// presenter("Arg"); // Fatal error 

$c1 = new Client;
$c1->prenom = "Bob";
$c1->nom = "Martin";
$c1->renomme(); //$c1->metNomEnMajuscules();
echo "<br>".$c1->nom;