<?php

class Voyage implements Iterator {
	public $nom, $prix, $etapes;
	private $taxe = 13.90;
	public function __construct($n = null, $p = 0) {
		$this->nom = $n;
		$this->prix = $p;
		$this->etapes = [];
	}
	public function get_prix() {
		return $this->prix + $this->taxe;
	}
	public function add_etape($e) {
		$this->etapes []= $e;
	}
	
	private $position = 0;
	public function current() { 
		return $this->etapes[$this->position]; 
	}
	public function key() { return $this->position; }
	public function next() { $this->position++; }
	public function rewind() { $this->position = 0; }
	public function valid() { 
		return $this->position < count($this->etapes); 
	}
}

$v1 = new Voyage("Fantastique Irlande", 829.90);
$v1->add_etape("Dublin");
$v1->add_etape("Cork");
$v1->add_etape("Tara");
$v1->add_etape("Cork");
echo "<h1>".$v1->nom."</h1><p>Seulement ".$v1->get_prix()."€</p>";
echo "<ul>";
foreach($v1 as $k=>$v)
	echo "<li>$k : $v";
echo "</ul>";

// ne fonctionne pas : un seul index de position
foreach($v1 as $k1=>$e1)
	foreach($v1 as $k2=>$e2)
		if($k1!=$k2 && $e1==$e2)
			echo "<h3>Retour à l'étape $e1</h3>";
		
function f1() {
	yield 0=>"A";
	yield 1=>"B";
}		
foreach(f1() as $v) echo $v; // affiche AB

// En irlande : 829.90 + 630.50 par voyageur additionnel
function lire_par_voyageurs($max) {
	for($n = 1 ; $n <= $max ; $n++) {
		yield $n => 829.90 + 630.50*($n-1);
	}
}
foreach(lire_par_voyageurs(5) as $n=>$p)
	echo "<br>$n voyageurs : $p €";




