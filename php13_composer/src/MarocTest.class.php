<?php
namespace Fr\ToulouseVoyage;

require "../vendor/autoload.php";
require "Maroc.class.php";

use PHPUnit\Framework\TestCase;

class MarocTest extends TestCase {
	public function test_get_prix() {
		$m = new Maroc;
		$m->voyageurs = 1;
		$m->jours = 1;
		$obtenu = $m->get_prix();
		$attendu = 530;
		$this->assertSame($attendu, $obtenu, "Calcul de get_prix incorrect");
		$m->voyageurs = 5;
		$m->jours = 10;
		$obtenu = $m->get_prix();		
		$attendu = 4000;
		$this->assertSame($attendu, $obtenu, "Calcul de get_prix incorrect");	
	}	
}