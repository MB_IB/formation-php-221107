<?php

function affiche_turquie(int $voyageurs) {
	if($voyageurs<=0)
		throw new Exception("Pas de voyageur");
	$prix = $voyageurs * 487;
	echo "<br>Turquie : ".$prix." €";
}

affiche_turquie(5);

try {
	affiche_turquie(0);
} catch(Exception $ex) {
	echo "<br>Exception !<br>".$ex;
} catch(Error $er) {
	echo "<br>Error !<br>".$er;
}

try {
	affiche_turquie("Ankara"); // Fatal error: Uncaught TypeError
} catch(Throwable $t) {
	echo "<br>Erreur ou Exception !<br>".$t;
}

echo "<br>Fin";
	