<?php
//phpinfo();

function verifie($s) {
	if(strpos($s, "<")!==false)
		throw Exception("Valeur inacceptable");
}

try {
	//$pdo = new PDO("sqlite:php14_pdo.sqlite");
	$pdo = new PDO("sqlite:php14_pdo.sqlite", null, null, 
		array( PDO::ATTR_PERSISTENT => true ));
	//$pdo = new PDO("mysql:host=192.168.1.22;dbname=mabase;port=3306","user","pass");

	$pdo->exec("CREATE TABLE IF NOT EXISTS reservations(".
		"id INTEGER PRIMARY KEY, nom TEXT, email TEXT, depart DATE)");
	
	$pdo->beginTransaction();
	
	if(count($_POST)>=3) {
		$nom = trim($_POST["nom"]);
		verifie($nom);
		$email = trim($_POST["email"]);
		verifie($email);
		$depart = trim($_POST["depart"]); //YYYY-MM-DD
		verifie($depart);
		$req = $pdo->prepare(
			"INSERT INTO reservations(nom, email, depart) VALUES (?,?,?)");
		$req->bindParam(1, $nom);
		$req->bindParam(2, $email);
		$req->bindParam(3, $depart);
		$req->execute();
		//NON : $pdo->exec(
		// "INSERT INTO reservations(nom, email, depart) VALUES ".
		// "('".$nom."','".$email."','".$depart."')");
	}
	
	$curseur = $pdo->query("SELECT * FROM reservations ORDER BY depart");
	$reservations = [];
	foreach($curseur as $r) {
		$reservations []= $r;
	}
	
	$pdo->commit();

} catch(PDOException $ex) {
	
	if(isset($pdo) && $pdo != null) 
		$pdo->rollback();
	
	exit("Erreur de connexion PDO : ".$ex);
}

?><html>
<body>
<form action="#" method="post">
	<p>Votre nom : <input name="nom" required></p>
	<p>Votre email : <input name="email" type="email" required></p>
	<p>Départ souhaité : <input name="depart" type="date" required></p>
	<p><input type="submit"></p>
</form>
<table>
<?php foreach($reservations as $r) { ?>
	<tr>
		<td><?= $r["nom"] ?></td>
		<td><?= $r["email"] ?></td>
		<td><?= $r["depart"] ?></td>
	</tr>
<?php } ?>
</table>
</body>
</html>