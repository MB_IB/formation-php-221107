<html><body><pre>
<?php
// reflexion
$a = 3;
var_dump($a); // int(8)
class Bateau {
	public $nom, $cabines;
	public function __construct() {
		$this->nom = "Titanic";
		$this->cabines = 239;		
	}
	public function afficher() {
		echo "<br>".$this->nom." (".$this->cabines." cabines)";
	}	
}

$b1 = new Bateau;
var_dump($b1);

if(is_int($b1)) echo "b1 est un entier\n";
if(is_object($b1)) echo "b1 est un objet\n";
echo gettype($b1)."\n";

echo get_class($b1)."\n"; // Bateau
if(class_exists(get_class($b1))) echo "La classe de b1 existe\n";
if(is_a($b1, "Bateau")) echo "b1 est un Bateau (ou classe fille)\n";
if(get_class($b1)=="Bateau") echo "b1 est un Bateau\n";

var_dump(get_class_vars(get_class($b1)));
var_dump(get_object_vars($b1));
var_dump(get_class_methods($b1));

$rc = new ReflectionClass(get_class($b1));
Reflection::export($rc);
if($rc->isAbstract()) echo "Abstraite!\n";



