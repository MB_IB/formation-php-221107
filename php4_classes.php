<?php

class Destination {
	private const DEFAUT_JOURS = 0;
	public static $balise = "<p>";
	public $nom, $pays, $jours;
	public function __construct($n = null, $p = null, 
			$j = self::DEFAUT_JOURS) {
		$this->nom = $n;
		$this->pays = $p;
		$this->jours = $j;
	}	
	public function __destruct() {
		echo "<br>Fin !";
	}
	public function afficher() {
		echo self::$balise.$this->nom." (".$this->pays.") : ".
			$this->jours." jours";
	}
	public function allonger($j = 1) {
		$this->jours += $j;
	}
	public function raccourcir($j = 1) {
		$this->allonger(-$j);
	}
}

$d1 = new Destination;
$d1->nom = "Alambra";
$d1->pays = "Espagne";
$d1->jours = 3;
$d1->afficher();
$d1->allonger(2);
$d1->raccourcir(1);
$d1->afficher(); // 4 jours

$d2 = new Destination("Camp Nou", "Espagne", 1);
$d2->afficher();
// $d2->balise = "<h3>"; // notice ! 
Destination::$balise = "<h3>";
$d2->afficher();

class DestinationMaritime extends Destination {
	public $ile;
	public function __construct($n = null, $p = null, 
			$j = self::DEFAUT_JOURS, $i = null) {
		parent::__construct($n, $p, $j);
		$this->ile = $i;
	}
	public function afficher() {
		parent::afficher();
		echo " - ".$this->ile;
	}
	public function allonger($j = 1) {
		parent::allonger($j);
		echo "<br>* ".$this->nom." allongé";
	}
}

$dm1 = new DestinationMaritime("Palma", "Espagne", 5);
$dm1->ile = "Mayorque";
$dm1->afficher();

$dm2 = new DestinationMaritime("Ibiza", "Espagne", 3, "Ibiza");
$dm2->afficher();
$dm2->raccourcir();


