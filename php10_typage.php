<?php
// declare(strict_types=1);

function get_prix_grece(int $voyageurs, int $jours) : ?float {
	if($voyageurs <= 0)
		return null;
	return $voyageurs * (640 + $jours * 38.50);
}

$p = get_prix_grece(2, 12);
echo $p."€ ";

$p = get_prix_grece("2", "12"); // conversion
echo $p."€ ";

$p = get_prix_grece(2.3, 12.8); // conversion / troncature
echo $p."€ ";

// $p = get_prix_grece("a", true); // erreur de conversion
// echo $p."€ ";

$p = get_prix_grece(0, 0); 
if($p!=null)
	echo $p."€ ";
else
	echo "Null";