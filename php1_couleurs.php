<?php
// affichage d'un code couleur, dans sa couleur
$message = "Indiquez un code couleur pour le logo de l'agence";
$style = "";
if(count($_GET)>=3) {
	$code = "#".$_GET["r"].$_GET["g"].$_GET["b"];
	if(strlen($code)==4 || strlen($code)==7) {
		$style = "font-size: 40pt; color:$code";
		$message = $code;		
	} else {
		$message = "Code incorrect";
	}
}
?><html>
<head>
	<meta charset="utf-8">
	<title>Choix de couleurs</title>
</head>
<body>
	<p style="<?= $style ?>"><?= $message ?></p>
	<form action="#">
		<p>R : <input name="r">,
		G : <input name="g">,
		B : <input name="b">
		<input type="submit"></p>
	</form>
</body>	
</html>