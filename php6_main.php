<?php

function chargeur_de_classe($nom_classe) {
	// echo "<p>Classe ".$nom_classe." manquante</p>";
	require_once("php6_".strtolower($nom_classe).".php");
}
spl_autoload_register("chargeur_de_classe");

$c1 = new Client;
$c1->nom = "Martin";
$c1->prenom = "Ann";
echo "Cliente : ".$c1->prenom." ".$c1->nom;

// https://voyages-toulouse.fr/croisiere
// -> https://voyages-toulouse.fr/index.php?page=croisiere