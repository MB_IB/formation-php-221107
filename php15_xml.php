<?php
/*
1 : req GET - paramètres d'URL
2 : req POST - corps de la requête - form-data / raw
3 : corps - CSV (fgetcsv/fputcsv), XML (DOM, SimpleXML), YAML, JSON (json_encode/json_decode)...
4 : streaming
*/
$app = simplexml_load_file("php15_xml.xml");
//echo "<pre>";
//var_dump($app);
echo "Nom du projet : ".$app->informations["nom"] ."<br>";
echo "1ère connexion : ".$app->connexions->connexion[0] ."<br>";
echo count($app->xpath("//connexion"))." connexions<br>";
echo count($app->xpath(
	"/application/connexions/connexion"))." connexions<br>";
echo $app->xpath("//connexion")[1]."<br>";
echo $app->xpath("//@nom")[1]."<br>"; //sqlite
echo $app->xpath("//connexion[@nom='sqlite']")[0]."<br>";
// modifications éventuelles
echo htmlentities($app->asXml());



	