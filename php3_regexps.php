<?php
// expressions rationelles

$desc = <<<END
Notre agence de voyage Toulouse Voyages est ouverte du lundi au samedi
de 11h à 18h. Elle propose des voyages en avion, vélo, train, pédalo, 
trou compris. Pour toutes informations, appelez notre numéro pas gratuit
0739849839 ou envoyer un email à contact@voyagestoulouse.fr.
END;

//if(mb_ereg_match(".*email.*", $desc)) // au début
if(preg_match("#.*email.*#u", $desc)) // au début
	echo "<p>\"email\" trouvé</p>";

//if(mb_ereg("\sa[a-z]*\s", $desc, $correspondances)) {
if(preg_match_all("#\sa[a-z]*\s#u", $desc, $correspondances)) { 
	echo "<p>Mots qui commencent par \"a\" trouvés<ul>";
	foreach($correspondances[0] as $c) {
		echo "<li>".$c;
	}
}

echo "</ul>".preg_replace("#Voyages#ui", "Travels", $desc);
/* Quantité :
    => 1
*   => 0 ou +
+   => 1 ou +
?   => 0 ou 1
{4} => 4
{3,6} => 3 à 6
*/

echo "<br>".preg_replace("#Voyages?#ui", "Travels", $desc);
/* Caractère :
a => a
\? => ?
\. => .
. => n'importe quoi
\s => espace, tabulation...
[abc] => a ou b ou c
[a-t] => a ou b ... ou t
[^abc] => n'importe quoi sauf a ou b ou c
*/

echo "<br>Sans tel : ".preg_replace("#[0-9\.\-]{10,14}#u", "**", $desc);

/* () : groupe */
echo "<br>".preg_replace("#([0-9]{1,2})h#ui", "$1 heures", $desc);







