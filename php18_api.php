<?php

header("Content-Type: application/json");

// https://.../php18_api.php?query=voyage/18/nom
if($_SERVER["REQUEST_METHOD"]=="GET") {
	if(isset($_GET["query"])) {
		$champs = mb_split("/", $_GET["query"]);
		$table = $champs[0];
		$id = $champs[1];
		$colonne = $champs[2];

		if($table=="voyage" && $id=18 && $colonne = "nom") {
			echo json_encode( ["resultat"=>"Randonnee andalouse"] );
		}		
	}
}